# Hello WebAssembly in Rust :crab:

This example is designed for compiling Rust libraries into WebAssembly.


## 🚴 Usage

```
cargo new my-wasm-library --lib
```


### 🛠️ Build with `wasm-pack build`

### use:

```
wasm-pack build --release --target TARGET -d dist/pkg-TARGET
```

### example:

```
wasm-pack build --release --target nodejs -d dist/pkg-nodejs
```


### generates the wasm file for:

```
wasm-pack build --target bundler # Outputs JS that is suitable for interoperation with a Bundler like Webpack. 
```

```
wasm-pack build --target web # Outputs JS that can be natively imported as an ES module in a browser, but the WebAssembly must be manually instantiated and loaded.
```

```
wasm-pack build --target no-modules # Same as web, except the JS is included on a page and modifies global state, and doesn't support as many wasm-bindgen features as web
```

```
wasm-pack build --target nodejs # Outputs JS that uses CommonJS modules, for use with a require statement. main key in package.json.
```


## 🎁🔋Make use of our JavaScript and Wasm:

- bundler: use **my_wasm_library.js** directly
- [web](docs/web_index.html)
- [no-modules](docs/no-modules_index.html)
- [nodejs](docs/node_index.js)


## 📚🔬 docs

- https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm
- https://opensource.com/article/19/3/calling-rust-javascript
- https://medium.com/geekculture/webassembly-for-node-js-13ef6bec0a0
- https://dev.to/dandyvica/wasm-in-rust-without-nodejs-2e0c 
- https://rustwasm.github.io/docs/wasm-pack/
- https://rustwasm.github.io/wasm-pack/book/commands/build.html#target
- https://blog.ryanlevick.com/posts/wasm-bindgen-interop/ 
