// Include the `wasm_bindgen` attribute into the current namespace.
use wasm_bindgen::prelude::wasm_bindgen;

// This attribute makes calling Rust from JavaScript possible.
// It generates code that can convert the basic types wasm understands
// (integers and floats) into more complex types like strings and
// vice versa. If you're interested in how this works, check this out:
// https://blog.ryanlevick.com/posts/wasm-bindgen-interop/
#[wasm_bindgen]
extern "C" {
    pub fn alert(s: &str);
}

#[wasm_bindgen]
pub fn excited_greeting(original: &str) -> String {
    //alert(&format!("Hello, {}!", original)); // comment this line, if it generates the wasm file for nodejs
    format!("HELLO, {}", original.to_uppercase())
}

#[wasm_bindgen]
pub fn reverse(s: String) -> String {
    s.chars().rev().collect::<String>()
}

#[wasm_bindgen]
pub fn sumar(x: u32, y: u32) -> u32 {
    x + y
}
