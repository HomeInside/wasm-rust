//import {excited_greeting, reverse} from "./my_wasm_library"; // ES6 version
const {excited_greeting, reverse, sumar} = require('./my_wasm_library');

console.log(excited_greeting("WebAssembly"));
console.log(reverse("WebAssembly"));
console.log(sumar(1, 2));
